Ai nevoie de Node.js instalat pe calculator
Porneste mai intai install.bat ca sa instalezi bot-ul.

Creezi o aplicatie pe discord aici: https://discord.com/developers/applications

O deschizi, te duci la tab-ul de Bot si adaugi un bot la aplicatie.

Copiaza token-ul de sub username si pune-l in token.js

Ca sa generezi codul de invitatia al botului, foloseste site-ul asta: 

https://discordapi.com/permissions.html#17835008

Trebuie sa aiba urmatoarele permisinui:
Read Message
Manage Messages
View Channel
Connect
Speak

-----------------------------------------
Lista Comenzi:

soundboard awake
soundboard injuri
soundboard hashinshin

manea plz (baga o manea la intamplare)

stop plz

manele repeat (manele pe repeat la intamplare)

stop manele repeat

gata ceasul

ora plz (intra si iti zice cat e ceasul)

netu @persoana (il da afara si spune un quote random din mesaje/cade_netu.txt)

-------------------------------------------

ora plz are optiunea de a pune un custom sound pentru cineva. In loc sa spuna ora, da play la mp3 ul repsectiv. ca sa faci asta, pui un mp3 cu numele de forma "id.mp3" in folderul audio/ora_easteregg.