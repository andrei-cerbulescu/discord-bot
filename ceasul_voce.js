module.exports.CeasulVoce = function (connection) {

    var date = new Date();
    var fisier = './audio/ore/' + date.getHours() + '.mp3';
    const dispatcher = connection.play('./audio/ore/este_ora.mp3');

    dispatcher.on('finish', () => {

        delete dispatcher;
        const dispatcher = connection.play(fisier);

        dispatcher.on('finish', () => {
            delete dispatcher;

            if (date.getMinutes() == 0) {

                const dispatcher = connection.play('./audio/ore/fix.mp3');
                dispatcher.on('finish', () => {
                    delete dispatcher;
                    connection.disconnect();
                });

            }

            else {

                const dispatcher = connection.play('./audio/ore/si.mp3');
                
                dispatcher.on('finish', () => {

                    delete dispatcher;

                    if (date.getMinutes() == 1) {

                        const dispatcher = connection.play('./audio/ore/un.mp3');
                        dispatcher.on('finish', () => {
                            delete dispatcher;
                            const dispatcher = connection.play('./audio/ore/minut.mp3');

                            dispatcher.on('finish', () => {
                                delete dispatcher;
                                connection.disconnect();
                            })

                        });

                    }

                    if (date.getMinutes() >= 2 && date.getMinutes() <= 19) {

                        fisier = './audio/ore/' + date.getMinutes() + '.mp3';

                        const dispatcher = connection.play(fisier);

                        dispatcher.on('finish', () => {
                            delete dispatcher;
                            final_singular(connection);
                        });

                    }

                    if (date.getMinutes() >= 20) {

                        fisier = './audio/ore/' + (date.getMinutes() - date.getMinutes() % 10) + '.mp3';

                        const dispatcher = connection.play(fisier);
                        dispatcher.on('finish', () => {
                            delete dispatcher;
                            if (date.getMinutes() % 10 === 0) final(connection);

                            else {

                                const dispatcher = connection.play('./audio/ore/si.mp3');

                                dispatcher.on('finish', () => {

                                    delete dispatcher;

                                    fisier = './audio/ore/' + date.getMinutes() % 10 + '.mp3';

                                    const dispatcher = connection.play(fisier);

                                    dispatcher.on('finish', () => {

                                        delete dispatcher;

                                        final(connection);

                                    });

                                });

                            }

                        });

                    }
                });
            }
        });

    });

}

function final(connection) {

    const dispatcher = connection.play('./audio/ore/de_minute.mp3');

    dispatcher.on('finish', () => {
        delete dispatcher;
        connection.disconnect();
    });

}

function final_singular(connection) {

    const dispatcher = connection.play('./audio/ore/minute.mp3');

    dispatcher.on('finish', () => {
        delete dispatcher;
        connection.disconnect();
    });

}