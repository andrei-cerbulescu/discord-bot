const RandomNumberClass = require('./random');

var audio_ghe=
[
    './audio/ghe/alin.mp3',
    './audio/ghe/andrei.m4a',
    './audio/ghe/dani.mp3',
    './audio/ghe/lupi.mp3',
    './audio/ghe/wd.mp3'
];

module.exports.GheRecursiv = function(connection){

    return new Promise(function(resolve, reject) {
        numar = audio_ghe[RandomNumberClass.RandomNumber(0, audio_ghe.length)];
        console.log(numar);
        const dispatcher = connection.play(numar);

        resolve(dispatcher);

    } );

}