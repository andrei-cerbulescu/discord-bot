const Discord = require('discord.js');
const RandomNumberClass = require('./random');
const ManeaRandomClass2 = require('./youtube.2');
const ClasaTokenLogin = require('./token_file');
const GheClass = require('./ghe');
const CeasulVoceClass = require('./ceasul_voce');
const fs = require('fs')
const ConnectionClass = require('./conexiuni_obiecte');
const CadeNetuClass = require('./cade_netu')

const client = new Discord.Client();

var ArrayConnections = [];

client.on('ready', () => {
  console.log('This bot is online!');
})

function manea_functie_repetitiva(connection) {

  ok = 1;

  ManeaRandomClass2.ManeaRandom2(connection, connection.channel.guild.id).then((dispatcher) => {

    connection.on('disconnect', () => {

      ok = 0;

    })

    dispatcher.on('finish', () => {

      if (ok === 1) {

        manea_functie_repetitiva(connection);

      }



    })
  });




}

function awake_functie_repetitiva(obiect) {

  const connection = obiect.message.member.voice.channel.join().then(connection => {

    var Delay_nou = RandomNumberClass.RandomNumber(180, 900);

    const dispatcher = connection.play('./audio/intro.mp3');
    dispatcher.setVolume(1.0);

    dispatcher.on('finish', () => {
      connection.disconnect();

      Delay_nou = 5;

      if (obiect.ok === 1) {

        setTimeout(() => {
          awake_functie_repetitiva(obiect);
        }, Delay_nou * 1000);

      }


      dispatcher.destroy();

    })

  });

}

client.login(ClasaTokenLogin.FunctieToken());

client.on('message', async message => {

  if (!message.guild) return;

  if (message.content === 'soundboard awake') {

    if (message.member.voice.channel) {
      console.log(message.member.user.username);
      message.delete();
      const connection = await message.member.voice.channel.join();
      const dispatcher = connection.play('./audio/intro.mp3');
      dispatcher.setVolume(1.0);


      dispatcher.on('finish', () => {
        connection.disconnect();
        dispatcher.destroy();
      })

    } else {
      message.reply('Awake trebuie sa intre pe un canal de voce!');
    }
  }

  if (message.content === 'soundboard hashinshin') {

    // Only try to join the sender's voice channel if they are in one themselves
    if (message.member.voice.channel) {
      console.log(message.member.user.username);
      message.delete();
      const connection = await message.member.voice.channel.join();
      const dispatcher = connection.play('./audio/hashinshin.wav');
      dispatcher.setVolume(1.0);


      dispatcher.on('finish', () => {
        connection.disconnect();
        dispatcher.destroy();
      })

    } else {
      message.reply('Awake trebuie sa intre pe un canal de voce!');
    }
  }

  if (message.content === 'soundboard injuri') {

    if (message.member.voice.channel) {
      console.log(message.member.user.username);
      message.delete();
      const connection = await message.member.voice.channel.join();
      const dispatcher = connection.play('./audio/injuri.mp3');
      dispatcher.setVolume(1.0);
      dispatcher.on('finish', () => {
        connection.disconnect();
        dispatcher.destroy();
      })

    } else {
      message.reply('Awake trebuie sa intre pe un canal de voce!');
    }
  }

  if (message.content === 'awake plz') {

    temp = new ConnectionClass.ConnectionClassConstructor(null, message.guild.id);
    temp.message = message;
    temp.ok = 1;
    ArrayConnections.push(temp);

    message.delete();
    awake_functie_repetitiva(temp);

  }

  if (message.content === 'awake stop') {
    message.delete();

    ArrayConnections.forEach(function (element) {

      if (element.id_server === message.guild.id) {

        element.ok = 0;

        ArrayConnections = ArrayConnections.filter(element_filtrare => element_filtrare.id_server != message.guild.id);

      }

    });
  }

  if (message.content === 'manea plz') {

    const connection = await message.member.voice.channel.join();

    ArrayConnections.push(new ConnectionClass.ConnectionClassConstructor(connection, message.guild.id));

    ManeaRandomClass2.ManeaRandom2(connection, message.guild.id).then((dispatcher) => {

      message.delete();

      dispatcher.on('finish', () => {

        ArrayConnections.forEach(function (element) {

          if (element.id_server === message.guild.id) {
    
            element.conexiune.disconnect();
    
            ArrayConnections = ArrayConnections.filter(element_filtrare => element_filtrare.id_server != message.guild.id);
    
          }
    
        });

        connection.disconnect();

      });
    });
  }

  if (message.content === 'manele repeat') {

    const connection = await message.member.voice.channel.join();

    ArrayConnections.push(new ConnectionClass.ConnectionClassConstructor(connection, message.guild.id));

    message.delete();

    manea_functie_repetitiva(connection);

  }

  if (message.content === 'stop plz') {

    message.delete();

    ArrayConnections.forEach(function (element) {

      if (element.id_server === message.guild.id) {

        element.conexiune.disconnect();

        ArrayConnections = ArrayConnections.filter(element_filtrare => element_filtrare.id_server != message.guild.id);

      }

    });

  }

  if (message.content === 'ora plz') {

    message.delete();

    if (fs.existsSync('./audio/ora_easteregg/' + message.member.id + '.mp3')) {


      const connection = await message.member.voice.channel.join();

      const dispatcher = connection.play('./audio/ora_easteregg/' + message.member.id + '.mp3');

      dispatcher.on('finish', () => {

        connection.disconnect();

      });

    }

    else {
      const connection = await message.member.voice.channel.join();
      CeasulVoceClass.CeasulVoce(connection);
    }
  }

  if(message.content.startsWith('netu')){

      CadeNetuClass.CadeNetu(message);
      //message.member.voice.setChannel(null);
      
  }

});