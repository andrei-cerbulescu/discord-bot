const fs = require('fs');
const ytlist = require('youtube-playlist');
const RandomNumberClass = require('./random');
const ytdl = require('ytdl-core');
const youtubedl = require('youtube-dl');
const getVideoId = require('get-video-id');
const { getVideoDurationInSeconds } = require('get-video-duration');
var rimraf = require("rimraf");

const url =
    ['https://www.youtube.com/playlist?list=PLuQ_PTV6xIkyyn7X7I2h_7ieSPqCVDoOe',
        'https://www.youtube.com/playlist?list=PLZWHoah8ZLx__ZDOfnbBDlpC8pM8jzrAM',
        'https://www.youtube.com/playlist?list=PLKyz__pOI1pxHzgDTL7Y4e45KD1deCwBC',
        'https://www.youtube.com/playlist?list=PLrRZajKPS1tB9GUG6oDiaA6qNmylW9Byt',
        'https://www.youtube.com/playlist?list=PLw01_Q2S_S3CFWxln3qBLEynxsXryCVAf'];

module.exports.ManeaRandom2 = function (connection, id_guild) {

    return new Promise(function (resolve, reject) {

        var pozitie_playlist = RandomNumberClass.RandomNumber(0, url.length - 1)
        console.log(pozitie_playlist);

        ytlist(url[pozitie_playlist], 'url').then(res => {
            var vector_manele = res.data.playlist;

            var pozitie = RandomNumberClass.RandomNumber(0, vector_manele.length - 1);

            console.log(pozitie);

            var manea = vector_manele[pozitie];

            console.debug(manea);

            if (manea === undefined) resolve(ManeaRandom2(connection));

            const video = youtubedl(manea,

                ['--format=18'],

                { cwd: __dirname });
            fs.mkdirSync('./audio/manele/' + id_guild, { recursive: true });
            video.pipe(fs.createWriteStream('./audio/manele/' + id_guild + '/' + getVideoId(manea).id));

            video.on('end', function () {
                console.log('finished downloading!')

                const dispatcher = connection.play('./audio/manele/' + id_guild + '/' + getVideoId(manea).id);

                getVideoDurationInSeconds('./audio/manele/' + id_guild + '/' + getVideoId(manea).id).then((duration) => {
                    setTimeout(() => {
                        fs.unlink('./audio/manele/' + id_guild + '/' + getVideoId(manea).id, (err) => {
                            if (err) console.log("Nu am putut sterge");
                        })

                    }, (duration + 1) * 1000);
                })

                connection.on('disconnect', ()=>{

                    rimraf('./audio/manele/' + id_guild, function () { console.log("done"); });

                });


                resolve(dispatcher);

            });

        });
    });
};
